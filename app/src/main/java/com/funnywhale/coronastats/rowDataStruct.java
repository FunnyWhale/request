package com.funnywhale.coronastats;

public class rowDataStruct {
    public String city;
    public String sick;
    public String healed;
    public String dead;


    public rowDataStruct(String argCity) {
        this.city = argCity;
    }
    public String getCity() {
        return city;
    }

    public String getDead() {
        return dead;
    }

    public String getHealed() {
        return healed;
    }

    public String getSick() {
        return sick;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setDead(String dead) {
        this.dead = dead;
    }

    public void setHealed(String healed) {
        this.healed = healed;
    }

    public void setSick(String sick) {
        this.sick = sick;
    }
}
