package com.funnywhale.coronastats;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.nodes.Document;
import org.jsoup.*;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;


public class WorldActivity extends AppCompatActivity {
    private TextView mTextViewResult;
    Document doc = null;
    String appendString = "";
    // boolean flag = false;
    int colOfElems = 224;
    int cols = 5;
    Map<String, String> mapCities = new HashMap<String, String>();
    ArrayList<rowDataStruct> rowDataStructs = new ArrayList<rowDataStruct>();

    class AsyncRequest extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.myProgressBar).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                findViewById(R.id.myProgressBar).setVisibility(View.INVISIBLE);
                TextView err = (TextView) findViewById(R.id.errorText);
                err.setText(result);
                err.setGravity(Gravity.CENTER);
                err.setVisibility(View.VISIBLE);
                return;
            }
            TableLayout myTable = (TableLayout) findViewById(R.id.myTable);
            findViewById(R.id.myProgressBar).setVisibility(View.INVISIBLE);

            //Заполнение таблицы
            for (int i = 0; i < colOfElems +
                    2; i++) {
                TableRow tableRow = new TableRow(WorldActivity.this);
                if (i % 2 != 0)
                    tableRow.setBackgroundColor(Color.parseColor("#4F0C80FD"));
                tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                for (int j = 0; j < cols; j++) {
                    TextView textView = new TextView(WorldActivity.this);
                    textView.setTextColor(Color.parseColor("#607080"));//464544
                    switch (i) {
                        case (0):
                            switch (j) {
                                case (0):
                                    textView.setText("");
                                    break;
                                case (1):
                                    textView.setText(" Место ");
                                    break;
                                case (2):
                                    textView.setText(" Зараженные ");
                                    break;
                                case (3):
                                    textView.setText(" Излечившиеся ");
                                    break;
                                case (4):
                                    textView.setText(" Умершие ");
                                    break;
                            }
                            textView.setTypeface(Typeface.DEFAULT_BOLD);
                            textView.setTextSize(14);
                            break;
                        case (1):
                            switch (j) {
                                case (0):
                                    textView.setText("");
                                    break;
                                case (1):
                                    textView.setText(" Всего ");
                                    break;
                                case (2):
                                    Integer sick = 0;
                                    for (rowDataStruct elem : rowDataStructs)
                                        //if (elem.getSick().compareTo("No data")!=0)
                                            sick += new Integer(elem.getSick().replace(",", ""));
                                    textView.setText(" " + sick + " ");
                                    break;
                                case (3):
                                    Integer healed = 0;
                                    for (rowDataStruct elem : rowDataStructs)
                                        if (elem.getHealed().compareTo("No data")!=0)
                                        healed += new Integer(elem.getHealed().replace(",", ""));
                                    textView.setText(" " + healed + " ");
                                    break;
                                case (4):
                                    Integer dead = 0;
                                    for (rowDataStruct elem : rowDataStructs)
                                        if (elem.getDead().compareTo("No data")!=0)
                                            dead += new Integer(elem.getDead().replace(",", ""));
                                    textView.setText(" " + dead + " ");
                                    break;
                            }
                            break;
                        default:
                            switch (j) {
                                case (0):
                                    textView.setText(" " + (i - 1) + ".");
                                    break;
                                case (1):
                                    textView.setText(" " + rowDataStructs.get(i - 2).getCity() + " ");
                                    break;
                                case (2):
                                    textView.setText(" " + rowDataStructs.get(i - 2).getSick() + " ");
                                    break;
                                case (3):
                                    textView.setText(" " + rowDataStructs.get(i - 2).getHealed() + " ");
                                    break;
                                case (4):
                                    textView.setText(" " + rowDataStructs.get(i - 2).getDead() + " ");
                                    break;
                            }
                            break;
                    }
                    tableRow.addView(textView, j);
                }
                myTable.addView(tableRow, i);

            }
            Toast.makeText(WorldActivity.this, "Источник: wikipedia.org", Toast.LENGTH_LONG).show();
        }

        @Override
        protected String doInBackground(String... arg) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            try {
                String[] stringArray = getResources().getStringArray(R.array.mapCities);
                for (String string : stringArray) {
                    String[] splitResult = string.split("\\|", 2);
                    mapCities.put(splitResult[0], splitResult[1]);
                }

                Connection connection = Jsoup.connect("https://en.wikipedia.org/wiki/Template:COVID-19_pandemic_data")
                        .userAgent("Chrome/4.0.249.0 Safari/532.5")
                        .referrer("http://www.google.com")
                        .timeout(100 * 1000);
                connection.timeout(100 * 1000);
                doc = connection.get();

                //Elements dbgList = doc.select("div.noresize > table > caption > span");

                Elements listElements = doc.select("table > tbody > tr ");
                Elements listCities = listElements.select("th > a");
                Elements listSick = listElements.select("td:nth-child(3)");
                Elements listHealed = listElements.select("td:nth-child(4)");
                Elements listDead = listElements.select("td:nth-child(5)");

                for (int i = 0; i < colOfElems; i++) {
                    rowDataStructs.add(new rowDataStruct(listCities.get(i).text()));
                }
                for (int i = 0; i < colOfElems; i++) {
                    rowDataStructs.get(i).setHealed(listHealed.get(i).text().replace(",", ""));
                }
                for (int i = 0; i < colOfElems; i++) {
                    rowDataStructs.get(i).setSick(listSick.get(i).text().replace(",", ""));
                }
                for (int i = 0; i < colOfElems; i++) {
                    rowDataStructs.get(i).setDead(listDead.get(i).text().replace(",", ""));
                }
                rowDataStructs.sort(new Comparator<rowDataStruct>() {
                    @Override
                    public int compare(rowDataStruct o1, rowDataStruct o2) {
                        return new Integer(o2.sick) - new Integer(o1.sick);
                    }
                });
                //flag = true;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "Ого! Что-то пошло не так.\nВозможно, COVID победил :)\nПопробуйте перезапустить приложение или проверить состояние сети.";
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WorldActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AsyncRequest().execute();
                //  while (!flag) { }
            }
        });

    }
}
